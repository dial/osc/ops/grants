Thank you for contact the Grants Team at the DIAL Open Source Center. We have 
received your message.

If you are receiving this message after submitting a grant application, there 
is no action required on your part. You may be contacted in case of any further 
questions, or to discuss next steps with your application upon its review.

If you have written to us with a question regarding a current grant round, or a 
general question about our grants, you will receive an update to this e-mail 
thread with a response. We strive to answer all questions within 3 business 
days (72 hours). 

Where appropriate, questions regarding a specific grant round are also 
generally published (after being anonymized) on the OSC Hub for the benefit of 
other applicants: https://hub.osc.dial.community/grants

Finally, if you need to withdraw your application, or would like to update us 
with new information or contact details, please simply reply to this message.

We appreciate your interest!

-- Governance Advisory Board, DIAL Open Source Center

Your application tracking ID: %{ISSUE_PATH}