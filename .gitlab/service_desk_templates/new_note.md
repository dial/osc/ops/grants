Your request to the Grants Team at the DIAL Open Source Center has been updated 
with the following information.

================================================================================

%{NOTE_TEXT}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Thank you for your continued interest in our program.

-- Governance Advisory Board, DIAL Open Source Center

================================================================================

NOTE: This is an update to your existing record. If you would like to inform us 
of any contact information or other changes, please reply to this email. Your 
application tracking ID is: %{ISSUE_PATH}